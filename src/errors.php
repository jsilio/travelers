<?php if (count($errors) > 0) : ?>
    <div class="error">
        <?php foreach ($errors as $error) : ?>
            <p class="text-sm text-center text-red-500"><?php echo $error ?></p>
        <?php endforeach ?>
    </div>
<?php endif ?>