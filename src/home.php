<?php
session_start();

if (!isset($_SESSION['id'])) {
    header('location: login.php');
}
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['id']);
    unset($_SESSION['role']);
    header("location: index.php");
}

include("db.php");

// Obtener datos del usuario
$email = $_SESSION['id'];
$resultadoUser = mysqli_query($db, "SELECT * FROM user WHERE email ='" . $email . "'");

$user = mysqli_fetch_assoc($resultadoUser);
mysqli_free_result($resultadoUser);


// Obtener datos de los posts
$resultadoPost = mysqli_query($db, "SELECT * FROM post");

$posts = mysqli_fetch_all($resultadoPost, MYSQLI_ASSOC);
mysqli_free_result($resultadoPost);

mysqli_close($db);

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Travel.ers — Feed</title>

    <link rel="stylesheet" href="styles/style.css">
</head>

<body>
    <div class="min-h-screen bg-gray-100">

        <header class="bg-white shadow-sm lg:static lg:overflow-y-visible">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div class="relative flex justify-between xl:grid xl:grid-cols-12 lg:gap-8">
                    <div class="flex md:absolute md:left-0 md:inset-y-0 lg:static xl:col-span-2">
                        <div class="flex-shrink-0 flex items-center">
                            <a href="home.php">
                                <h1 class="text-4xl font-semibold text-indigo-700 inline">Travel.ers</h1>
                            </a>
                        </div>
                    </div>
                    <div class="min-w-0 flex-1 md:px-8 lg:px-0 xl:col-span-6">
                        <div class="flex items-center px-6 py-4 md:max-w-3xl md:mx-auto lg:max-w-none lg:mx-0 xl:px-0">
                            <div class="w-full">
                                <label for="search" class="sr-only">Buscar</label>
                                <div class="relative">
                                    <div class="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center">
                                        <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
                                        </svg>
                                    </div>
                                    <input id="search" name="search" class="block w-full bg-white border border-gray-300 rounded-md py-2 pl-10 pr-3 text-sm placeholder-gray-500 focus:outline-none focus:text-gray-900 focus:placeholder-gray-400 focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm" placeholder="Buscar" type="search">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex items-center md:absolute md:right-0 md:inset-y-0 lg:hidden">

                        <button type="button" class="-mx-2 rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500" aria-expanded="false">
                            <span class="sr-only">Open menu</span>

                            <svg class="block h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                            </svg>

                            <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                        </button>
                    </div>

                    <!-- Información del usuario -->
                    <div class="hidden lg:flex lg:items-center lg:justify-end xl:col-span-4">
                        <?php if ($user) : ?>
                            <!-- Nombre -->
                            <span class="text-indigo-400"><?php echo htmlspecialchars($user['first_name'] . " " . $user['last_name']); ?></span>
                            <!-- Avatar -->
                            <div class="flex-shrink-0 relative ml-3">
                                <div>
                                    <a href="configuracion.php">
                                        <img class="h-11 w-11 rounded-full" src='uploads/<?php echo $user["avatar"] ?>' alt="Avatar">
                                    </a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <a href="index.php?logout='1'" class="ml-8 inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Cerrar sesión
                        </a>
                    </div>
                </div>
            </div>

        </header>

        <div class="py-10">
            <div class="max-w-3xl mx-auto sm:px-6 lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-12 lg:gap-8">
                <div class="hidden lg:block lg:col-span-3 xl:col-span-2">
                    <nav aria-label="Sidebar" class="sticky top-4 divide-y divide-gray-300">
                        <div class="pb-8 space-y-1">
                            <!-- Current: "bg-gray-200 text-gray-900", Default: "text-gray-600 hover:bg-gray-50" -->
                            <a href="#" class="bg-gray-200 text-gray-900 group flex items-center px-3 py-2 text-sm font-medium rounded-md" aria-current="page">
                                <!-- Heroicon name: outline/home -->
                                <svg class="text-gray-500 flex-shrink-0 -ml-1 mr-3 h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                                </svg>
                                <span class="truncate">
                                    Inicio
                                </span>
                            </a>

                            <a href="#" class="text-gray-600 hover:bg-gray-50 group flex items-center px-3 py-2 text-sm font-medium rounded-md">
                                <!-- Heroicon name: outline/fire -->
                                <svg class="text-gray-400 group-hover:text-gray-500 flex-shrink-0 -ml-1 mr-3 h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 18.657A8 8 0 016.343 7.343S7 9 9 10c0-2 .5-5 2.986-7C14 5 16.09 5.777 17.656 7.343A7.975 7.975 0 0120 13a7.975 7.975 0 01-2.343 5.657z" />
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.879 16.121A3 3 0 1012.015 11L11 14H9c0 .768.293 1.536.879 2.121z" />
                                </svg>
                                <span class="truncate">
                                    Popular
                                </span>
                            </a>

                            <a href="#" class="text-gray-600 hover:bg-gray-50 group flex items-center px-3 py-2 text-sm font-medium rounded-md">
                                <!-- Heroicon name: outline/user-group -->
                                <svg class="text-gray-400 group-hover:text-gray-500 flex-shrink-0 -ml-1 mr-3 h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                                </svg>
                                <span class="truncate">
                                    Comunidades
                                </span>
                            </a>

                            <a href="#" class="text-gray-600 hover:bg-gray-50 group flex items-center px-3 py-2 text-sm font-medium rounded-md">
                                <!-- Heroicon name: outline/trending-up -->
                                <svg class="text-gray-400 group-hover:text-gray-500 flex-shrink-0 -ml-1 mr-3 h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 7h8m0 0v8m0-8l-8 8-4-4-6 6" />
                                </svg>
                                <span class="truncate">
                                    Trending
                                </span>
                            </a>
                        </div>
                        <div class="pt-10">
                            <p class="px-3 text-xs font-semibold text-gray-500 uppercase tracking-wider" id="communities-headline">
                                Países en Tendencias
                            </p>
                            <div class="mt-3 space-y-2" aria-labelledby="communities-headline">
                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        Tailandia
                                    </span>
                                </a>

                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        México
                                    </span>
                                </a>

                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        España
                                    </span>
                                </a>

                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        Vietnam
                                    </span>
                                </a>

                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        Italia
                                    </span>
                                </a>

                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        Indonesia
                                    </span>
                                </a>

                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        Estados Unidos
                                    </span>
                                </a>

                                <a href="#" class="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50">
                                    <span class="truncate">
                                        Laos
                                    </span>
                                </a>
                            </div>
                        </div>
                    </nav>
                </div>
                <main class="lg:col-span-9 xl:col-span-6">
                    <div class="px-4 sm:px-0">

                        <div class="hidden sm:block">
                            <h1 class="text-indigo-500 font-semibold text-3xl">Posts recientes</h1>
                        </div>
                    </div>
                    <div class="mt-4">
                        <h1 class="sr-only">Recent questions</h1>
                        <ul class="space-y-4">
                            <?php foreach ($posts as $post) { ?>
                                <li class="bg-white px-4 py-6 shadow sm:p-6 sm:rounded-lg">
                                    <article>
                                        <div>
                                            <div class="flex space-x-3">
                                                <div class="flex-shrink-0">
                                                    <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixqx=fkyAXz1111&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                                </div>
                                                <div class="min-w-0 flex-1">
                                                    <p class="text-sm font-medium text-gray-900">
                                                        <?php echo htmlspecialchars($post['author']); ?>
                                                    </p>
                                                    <p class="text-sm text-gray-500">
                                                        <a href="#" class="hover:underline">
                                                            <time datetime="2020-12-09T11:43:00">hace 30m</time>
                                                        </a>
                                                    </p>
                                                </div>

                                            </div>
                                            <h2 class="mt-4 text-xl font-semibold text-gray-900">
                                                <?php echo htmlspecialchars($post['title']); ?>
                                            </h2>
                                        </div>
                                        <div class="mt-2 text-sm text-gray-700 space-y-4">
                                            <p><?php echo htmlspecialchars($post['description']); ?></p>
                                        </div>
                                        <div class="mt-6 flex justify-between space-x-8">
                                            <div class="flex space-x-6">
                                                <span class="inline-flex items-center text-sm">
                                                    <button class="inline-flex space-x-2 text-gray-400 hover:text-gray-500">

                                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path d="M2 10.5a1.5 1.5 0 113 0v6a1.5 1.5 0 01-3 0v-6zM6 10.333v5.43a2 2 0 001.106 1.79l.05.025A4 4 0 008.943 18h5.416a2 2 0 001.962-1.608l1.2-6A2 2 0 0015.56 8H12V4a2 2 0 00-2-2 1 1 0 00-1 1v.667a4 4 0 01-.8 2.4L6.8 7.933a4 4 0 00-.8 2.4z" />
                                                        </svg>
                                                        <span class="font-medium text-gray-900"><?php echo htmlspecialchars($post['likes']); ?></span>

                                                    </button>
                                                </span>
                                                <span class="inline-flex items-center text-sm">
                                                    <button class="inline-flex space-x-2 text-gray-400 hover:text-gray-500">

                                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path fill-rule="evenodd" d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z" clip-rule="evenodd" />
                                                        </svg>
                                                        <span class="font-medium text-gray-900"><?php echo htmlspecialchars($post['comments']); ?></span>

                                                    </button>
                                                </span>
                                                <span class="inline-flex items-center text-sm">
                                                    <button class="inline-flex space-x-2 text-gray-400 hover:text-gray-500">

                                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path d="M10 12a2 2 0 100-4 2 2 0 000 4z" />
                                                            <path fill-rule="evenodd" d="M.458 10C1.732 5.943 5.522 3 10 3s8.268 2.943 9.542 7c-1.274 4.057-5.064 7-9.542 7S1.732 14.057.458 10zM14 10a4 4 0 11-8 0 4 4 0 018 0z" clip-rule="evenodd" />
                                                        </svg>
                                                        <span class="font-medium text-gray-900"><?php echo htmlspecialchars($post['views']); ?></span>

                                                    </button>
                                                </span>
                                            </div>
                                            <div class="flex text-sm">
                                                <span class="inline-flex items-center text-sm">
                                                    <button class="inline-flex space-x-2 text-gray-400 hover:text-gray-500">
                                                        <!-- Heroicon name: solid/share -->
                                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path d="M15 8a3 3 0 10-2.977-2.63l-4.94 2.47a3 3 0 100 4.319l4.94 2.47a3 3 0 10.895-1.789l-4.94-2.47a3.027 3.027 0 000-.74l4.94-2.47C13.456 7.68 14.19 8 15 8z" />
                                                        </svg>
                                                        <span class="font-medium text-gray-900">Compartir</span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </article>
                                </li>

                            <?php } ?>

                        </ul>
                    </div>
                </main>
                <aside class="hidden xl:block xl:col-span-4">
                    <div class="sticky top-4 space-y-4">
                        <section aria-labelledby="who-to-follow-heading">
                            <div class="bg-white rounded-lg shadow">
                                <div class="p-6">
                                    <h2 id="who-to-follow-heading" class="text-lg font-semibold text-gray-900">
                                        A quién seguir
                                    </h2>
                                    <div class="mt-6 flow-root">
                                        <ul class="-my-4 divide-y divide-gray-200">
                                            <li class="flex items-center py-4 space-x-3">
                                                <div class="flex-shrink-0">
                                                    <img class="h-8 w-8 rounded-full" src="https://pbs.twimg.com/profile_images/1315508432431190016/r1NQzfyE_400x400.jpg" alt="">
                                                </div>
                                                <div class="min-w-0 flex-1">
                                                    <p class="text-sm font-medium text-gray-900">
                                                        <a href="#">El Pepe</a>
                                                    </p>
                                                    <p class="text-sm text-gray-500">
                                                        <a href="#">@elpepe</a>
                                                    </p>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <button type="button" class="inline-flex items-center px-3 py-0.5 rounded-full bg-indigo-50 text-sm font-medium text-indigo-700 hover:bg-indigo-100">
                                                        <!-- Heroicon name: solid/plus -->
                                                        <svg class="-ml-1 mr-0.5 h-5 w-5 text-indigo-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                                                        </svg>
                                                        <span>
                                                            Seguir
                                                        </span>
                                                    </button>
                                                </div>
                                            </li>

                                            <li class="flex items-center py-4 space-x-3">
                                                <div class="flex-shrink-0">
                                                    <img class="h-8 w-8 rounded-full" src="https://phantom-marca.unidadeditorial.es/46f1d153f0acdb7e24b23927a0dfa66c/crop/0x439/1078x1045/resize/1320/f/jpg/assets/multimedia/imagenes/2021/06/03/16227320190724.jpg" alt="">
                                                </div>
                                                <div class="min-w-0 flex-1">
                                                    <p class="text-sm font-medium text-gray-900">
                                                        <a href="#">Gabriel Chachi</a>
                                                    </p>
                                                    <p class="text-sm text-gray-500">
                                                        <a href="#">@gabrielchachi</a>
                                                    </p>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <button type="button" class="inline-flex items-center px-3 py-0.5 rounded-full bg-indigo-50 text-sm font-medium text-indigo-700 hover:bg-indigo-100">
                                                        <!-- Heroicon name: solid/plus -->
                                                        <svg class="-ml-1 mr-0.5 h-5 w-5 text-indigo-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                                                        </svg>
                                                        <span>
                                                            Seguir
                                                        </span>
                                                    </button>
                                                </div>
                                            </li>

                                            <li class="flex items-center py-4 space-x-3">
                                                <div class="flex-shrink-0">
                                                    <img class="h-8 w-8 rounded-full" src="https://www.xlsemanal.com/wp-content/uploads/sites/3/2017/10/personajes-frank-de-la-jungla-animales-que-dan-miedo-naturaleza-xlsemanal-2.jpg" alt="">
                                                </div>
                                                <div class="min-w-0 flex-1">
                                                    <p class="text-sm font-medium text-gray-900">
                                                        <a href="#">Frank de la Jungla</a>
                                                    </p>
                                                    <p class="text-sm text-gray-500">
                                                        <a href="#">@junglefrank</a>
                                                    </p>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <button type="button" class="inline-flex items-center px-3 py-0.5 rounded-full bg-indigo-50 text-sm font-medium text-indigo-700 hover:bg-indigo-100">
                                                        <!-- Heroicon name: solid/plus -->
                                                        <svg class="-ml-1 mr-0.5 h-5 w-5 text-indigo-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd" />
                                                        </svg>
                                                        <span>
                                                            Seguir
                                                        </span>
                                                    </button>
                                                </div>
                                            </li>

                                            <!-- More people... -->
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </section>
                        <section aria-labelledby="trending-heading">
                            <div class="bg-white rounded-lg shadow">
                                <div class="p-6">
                                    <h2 id="trending-heading" class="text-lg font-semibold text-gray-900">
                                        Tendencias
                                    </h2>
                                    <div class="mt-6 flow-root">
                                        <ul class="-my-4 divide-y divide-gray-200">
                                            <li class="flex py-4 space-x-3">
                                                <div class="flex-shrink-0">
                                                    <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1463453091185-61582044d556?ixlib=rb-1.2.1&ixqx=fkyAXz1111&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="Floyd Miles">
                                                </div>
                                                <div class="min-w-0 flex-1">
                                                    <p class="text-sm text-gray-800">¿Cuánto cuesta viajar a Japón? Presupuesto y recomendaciones</p>
                                                    <div class="mt-2 flex">
                                                        <span class="inline-flex items-center text-sm">
                                                            <button class="inline-flex space-x-2 text-gray-400 hover:text-gray-500">
                                                                <!-- Heroicon name: solid/chat-alt -->
                                                                <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                                    <path fill-rule="evenodd" d="M18 5v8a2 2 0 01-2 2h-5l-5 4v-4H4a2 2 0 01-2-2V5a2 2 0 012-2h12a2 2 0 012 2zM7 8H5v2h2V8zm2 0h2v2H9V8zm6 0h-2v2h2V8z" clip-rule="evenodd" />
                                                                </svg>
                                                                <span class="font-medium text-gray-900">291</span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </li>

                                            <!-- More posts... -->
                                        </ul>
                                    </div>
                                    <div class="mt-6">
                                        <a href="#" class="w-full block text-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50">
                                            Ver más
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</body>

</html>