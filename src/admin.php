<?php
session_start();

if (!isset($_SESSION['id']) || !isset($_SESSION['role'])) {
    header('location: login.php');
}

if ($_SESSION['role'] != 'admin') {
    header('location: login.php');
}

include("db.php");

// Obtener datos de los posts
$resultado = mysqli_query($db, "SELECT * FROM user");

$users = mysqli_fetch_all($resultado, MYSQLI_ASSOC);
mysqli_free_result($resultado);

mysqli_close($db);
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Panel - Travel.ers</title>
    <link rel="stylesheet" href="styles/style.css">
</head>

<body>


    <div class="relative min-h-screen bg-gradient-to-tr from-pink-50 via-purple-50 to-indigo-100 pb-16  overflow-hidden">
        <main class="relative mt-12">
            <div class="max-w-screen-xl mx-auto pb-6 px-4 sm:px-6 lg:pb-10 lg:px-8">
                <!-- Título -->
                <h1 class="text-5xl font-semibold mb-3 text-indigo-700">Travel.ers</h1>
                <h1 class="text-3xl font-semibold mb-14">
                    Administración de usuarios
                </h1>

                <!-- Tabla de usuarios -->
                <div class="flex flex-col">
                    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <thead class="bg-gray-50">
                                        <tr>
                                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Nombre completo
                                            </th>
                                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                E-mail
                                            </th>
                                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Status
                                            </th>
                                            <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                                Rol
                                            </th>
                                            <th scope="col" class="relative px-6 py-3">
                                                <span class="sr-only">Edit</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        <?php foreach ($users as $user) { ?>
                                            <tr>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="flex items-center">
                                                        <div class="flex-shrink-0 h-10 w-10">
                                                            <img class="h-10 w-10 rounded-full" src='uploads/<?php echo $user["avatar"] ?>' alt="Avatar">
                                                        </div>
                                                        <div class="ml-4">
                                                            <div class="text-sm font-medium text-gray-900">
                                                                <?php echo htmlspecialchars($user['first_name'] . " " . $user['last_name']); ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <div class="text-sm text-gray-900"> <?php echo htmlspecialchars($user['email']); ?></div>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap">
                                                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                                                        Activo
                                                    </span>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-sm capitalize text-gray-500">
                                                    <?php echo htmlspecialchars($user['role']); ?>
                                                </td>
                                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                    <a href="#" class="text-indigo-600 hover:text-indigo-900">Editar</a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>


</body>

</html>