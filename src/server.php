  
<?php
include('db.php');
session_start();

$errors = array();



// Registrar usuario
if (isset($_POST['reg_user']) && isset($_FILES['avatar'])) {

    // Recibir todos los valores del formulario
    $first_name = mysqli_real_escape_string($db, $_POST['first_name']);
    $last_name = mysqli_real_escape_string($db, $_POST['last_name']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    // Validación de formulario.
    if (empty($first_name)) {
        array_push($errors, "El nombre es obligatorio.");
    }
    if (empty($last_name)) {
        array_push($errors, "El apellido es obligatorio.");
    }
    if (empty($email)) {
        array_push($errors, "El email es obligatorio.");
    }
    if (empty($password)) {
        array_push($errors, "La contraseá es obligatoria.");
    }

    // Verificar en la base de datos que no existe un usuario con el mismo email
    $user_check_query = "SELECT * FROM user WHERE email='$email' LIMIT 1";
    $result = mysqli_query($db, $user_check_query);
    $user = mysqli_fetch_assoc($result);

    if ($user) {
        if ($user['email'] === $email) {
            array_push($errors, "Ya hay un usuario registrado con este correo electrónico.");
        }
    }

    // Registrar si no hay errores
    if (count($errors) == 0) {

        $role = "usuario";

        // Encriptar contraseñar (cambiar MD5)
        $encrypted_password = md5($password);

        // Obtener información de avatar
        $avatar = $_FILES['avatar']['name'];
        $target = "uploads/" . basename($avatar);

        $query = "INSERT INTO user(first_name, last_name, email, password, avatar, role) 
  			  VALUES('$first_name', '$last_name', '$email', '$encrypted_password', '$avatar', '$role');";

        mysqli_query($db, $query) or die(mysqli_error($db));;

        if (move_uploaded_file($_FILES['avatar']['tmp_name'], $target)) {
            $_SESSION['id'] = $email;
            header('location: login.php');
            exit();
        }
    }
}

// Login
if (isset($_POST['login_user'])) {
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    if (empty($email)) {
        array_push($errors, "Debes indicar tu correo electrónico.");
    }
    if (empty($password)) {
        array_push($errors, "Debes indicar tu contraseña.");
    }

    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT * FROM user WHERE email='$email' AND password='$password'";
        $results = mysqli_query($db, $query);
        if (mysqli_num_rows($results) == 1) {
            $_SESSION['id'] = $email;
            $fila = mysqli_fetch_assoc($results);
            $_SESSION['role'] = $fila['role'];
            header('location: home.php');
        } else {
            array_push($errors, "Los datos de acceso son incorrectos.");
        }
    }
}

// Modificar datos del usuario
if (isset($_POST['modify_user'])) {
    // Obtener datos del formulario
    $first_name = mysqli_real_escape_string($db, $_POST['first_name']);
    $last_name = mysqli_real_escape_string($db, $_POST['last_name']);
    $new_password = mysqli_real_escape_string($db, $_POST['new_password']);
    $email = $_SESSION['id'];
    // Encriptar contraseña para verificar
    $password = mysqli_real_escape_string($db, $_POST['password']);
    $encrypted_password = md5($password);
    $query = "SELECT * FROM user WHERE email='$email' AND password='$encrypted_password'";
    $results = mysqli_query($db, $query) or die(mysqli_error($db));
    // Si la contraseña es correcta
    if (mysqli_num_rows($results) == 1) {
        $avatar = $_FILES['avatar']['name'];
        $target = "uploads/" . basename($avatar);
        $new_email = mysqli_real_escape_string($db, $_POST['email']);
        $encrypted_password = md5($new_password);
        // Update
        $update_query = "UPDATE user 
        SET first_name = '$first_name', 
        last_name = '$last_name',
         email = '$new_email', 
         avatar = '$avatar',
         password = '$new_password' 
        WHERE email ='$email';";
        mysqli_query($db, $update_query) or die(mysqli_error($db));;
        if (move_uploaded_file($_FILES['avatar']['tmp_name'], $target)) {

            $_SESSION['id'] = $email;
            header('location: configuracion.php');
            exit();
        }
    } else {
        array_push($errors, "La contraseña es incorrecta.");
    }
}

?>