# Travel.ers

Travel.ers es una red social para viajeros y nómadas digitales.

### Stack

- **Estilos**: [TailwindCSS](https://tailwindcss.com/).
- **JS Framework**: [Alpine](https://github.com/alpinejs/alpine). (utilizado para un modal)
- **Backend**: PHP y MySQL (phpMyAdmin)
